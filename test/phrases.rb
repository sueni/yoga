require 'minitest/autorun'
require 'tempfile'
require_relative '../lib/phrases'

tf = Tempfile.new
tf.write <<~EOF
    # ЧЕМ

    о чем   -> о чём
    в нем   -> в нём
    вот и все. -> вот и всё.
  EOF
tf.close

describe Phrases do
  before do
    @ph = Phrases.new(tf.path)
  end

  it 'chould return array of regexps and substitutions' do
    [
      [/\bо\ чем\b/, 'о чём'],
      [/\bО\ чем\b/, 'О чём'],
      [/\bв\ нем\b/, 'в нём'],
      [/\bВ\ нем\b/, 'В нём'],
      [/\bвот\ и\ все\./, 'вот и всё.'],
      [/\bВот\ и\ все\./, 'Вот и всё.'],
    ].must_equal @ph.regexps
  end

  it 'should apply all replacements' do
    @ph.replace('О чем?').must_equal 'О чём?'
  end
end
