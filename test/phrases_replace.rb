require 'minitest/autorun'
require_relative '../lib/phrases'
require_relative '../lib/string_helper'

ph = Phrases.new(File.join(File.dirname(__FILE__),
                            '../dict/phrases.conf'))
def ph.replace(string)
  StringHelper.clean_forced_e_marks(super)
end

describe 'phrases replacements' do
  it 'ЧЁМ' do
    ph.replace('о чем').must_equal 'о чём'
    ph.replace('не о чём').must_equal 'не о чем'
    ph.replace('не о чем-либо').must_equal 'не о чём-либо'
    ph.replace('более чем').must_equal 'более чем'
    ph.replace('не над чем').must_equal 'не над чем'
    ph.replace('не в чем').must_equal 'не в чем'
    ph.replace('не в чем ином').must_equal 'не в чём ином'
    ph.replace('ни в чем').must_equal 'ни в чём'
    ph.replace('в чем-либо').must_equal 'в чём-либо'
    ph.replace('при чем').must_equal 'при чём'
    ph.replace('в чем дело').must_equal 'в чём дело'
    ph.replace('В чем же дело').must_equal 'В чём же дело'
    ph.replace('В чем именно').must_equal 'В чём именно'
    ph.replace('В чём я сомневаюсь').must_equal 'В чём я сомневаюсь'
    ph.replace('В чём?').must_equal 'В чём?'
    ph.replace('В чём:').must_equal 'В чём:'
    ph.replace('То, в чем более').must_equal 'То, в чём более'
    ph.replace('Чем в прошлом').must_equal 'Чем в прошлом'
    ph.replace('чем те,').must_equal 'чем те,'
    ph.replace('О чем те говорят').must_equal 'О чём те говорят'
    ph.replace('чем до').must_equal 'чем до'
  end


  it 'МОЁМ' do
    ph.replace('в моем').must_equal 'в моём'
    ph.replace('о моем').must_equal 'о моём'
    ph.replace('на моем').must_equal 'на моём'
  end

  it 'НЁМ' do
    ph.replace('в нем').must_equal 'в нём'
    ph.replace('о нем').must_equal 'о нём'
    ph.replace('при нем').must_equal 'при нём'
    ph.replace('на нем').must_equal 'на нём'
  end

  it 'ВСЁМ' do
    assert_equal 'обо всём', ph.replace('обо всем')
    assert_equal 'во всём', ph.replace('во всем')
    assert_equal 'над всем', ph.replace('над всем')
    assert_equal 'по всем', ph.replace('по всем')
    assert_equal 'ним всем', ph.replace('ним всем')
  end

  it 'ВСЁ' do
    ph.replace('всем том').must_equal 'всём том'
    ph.replace('при всем').must_equal 'при всём'
    ph.replace('все-таки').must_equal 'всё-таки'
    ph.replace('все время').must_equal 'всё время'
    ph.replace('все кончено').must_equal 'всё кончено'
    ph.replace('все будет').must_equal 'всё будет'
    ph.replace('все зависит').must_equal 'всё зависит'
    ph.replace('все остальное').must_equal 'всё остальное'
    ph.replace('всем остальном').must_equal 'всём остальном'
    ph.replace('на всё про всё').must_equal 'на всё про всё'
    ph.replace('понимают ли они все то, что')
      .must_equal 'понимают ли они всё то, что'
    ph.replace('кто дал все это?').must_equal 'кто дал всё это?'
    ph.replace('Все еще жив').must_equal 'Всё ещё жив'
    ph.replace('ко все более').must_equal 'ко всё более'
  end

  it 'UNSORTED' do
    ph.replace('черт возьми').must_equal 'чёрт возьми'
    ph.replace('черт побери').must_equal 'чёрт побери'
    ph.replace('темно-красный').must_equal 'тёмно-красный'
    ph.replace('В жены ему').must_equal 'В жёны ему'
    ph.replace('Темным-темно').must_equal 'Темным-темно'
    ph.replace('Далеко до него').must_equal 'Далеко до него'
    ph.replace('О жилье').must_equal 'О жилье'
    ph.replace('На счету').must_equal 'На счету'
    ph.replace('Хай-тек').must_equal 'Хай-тек'
  end

  it 'tests questionable cases' do
    ph.replace('вот и все.').must_equal 'вот и всё.'
    ph.replace('вот и все!').must_equal 'вот и всё!'
    ph.replace('все равно.').must_equal 'всё равно.'
    ph.replace('все равно,').must_equal 'всё равно,'
  end

  it 'ВСЕГДА Е' do
    ph.replace('с чем').must_equal 'с чем'
    ph.replace('со всем').must_equal 'со всем'
    ph.replace('чем то').must_equal 'чем то'
    ph.replace('не чем иным').must_equal 'не чем иным'
    ph.replace('со счетов').must_equal 'со счетов'
    ph.replace('Над чем').must_equal 'Над чем'
    ph.replace('Все эти игры').must_equal 'Все эти игры'
  end
end

describe 'keep untoched corner cases' do
  it 'ВСЕ ЖЕ' do
    source = ('все же не могут быть, как ты')
    ph.replace(source).must_equal source
  end
end
