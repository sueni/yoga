require 'minitest/autorun'
require_relative '../lib/yo_proxy'

describe YoProxy do
  before do
    @yo = YoProxy.new({
      'foo' => 'bar',
      'четки' => 'чётки',
      'преклонен' => 'преклонён',
      'чем' => '?чём',
      'рожденное' => 'рождённое',
    })
  end

  it 'should extrack value by the key' do
    @yo['foo'].must_equal 'bar'
    @yo['четки'].must_equal 'чётки'
  end

  it 'should set value by the key' do
    @yo['foo'] = 'baz'
    @yo['foo'].must_equal 'baz'
  end

  it 'should delete value by the key' do
    @yo.delete('foo')
    @yo['foo'].must_be_nil
  end

  it 'should return variant of ne-word' do
    @yo['нечетки'].must_equal 'нечётки'
  end

  it 'should consider prefix exclusions' do
    @yo['непреклонен'].must_equal 'непреклонен'
  end

  it 'should return ambiguous value unchanged' do
    @yo['нечем'].must_equal 'нечем'
  end

  it 'САМО' do
    @yo['саморожденное'].must_equal 'саморождённое'
  end
end
