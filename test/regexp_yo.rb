require 'minitest/autorun'
require_relative '../lib/regexp_yo'

describe RegexpYo do
  it 'replaces "трех" with "трёх"' do
    RegexpYo.new('трех').trex_chet.must_equal 'трёх'
    RegexpYo.new('Трех').trex_chet.must_equal 'Трёх'
    RegexpYo.new(' трех ').trex_chet.must_equal ' трёх '
    RegexpYo.new('трехголовый').trex_chet.must_equal 'трёхголовый'
    RegexpYo.new('двух-трех').trex_chet.must_equal 'двух-трёх'
  end

  it 'replaces "четырех" with "четырёх"' do
    RegexpYo.new('четырех').trex_chet.must_equal 'четырёх'
    RegexpYo.new('Четырех').trex_chet.must_equal 'Четырёх'
    RegexpYo.new(' четырех ').trex_chet.must_equal ' четырёх '
    RegexpYo.new('четырехголовый').trex_chet.must_equal 'четырёхголовый'
    RegexpYo.new('трех-четырех').trex_chet.must_equal 'трёх-четырёх'
  end

  it 'runs all the conversions' do
    source = 'Трехкопеечный Четырехсантиметровый Калоприемник'
    expect = 'Трёхкопеечный Четырёхсантиметровый Калоприёмник'
    RegexpYo.turn_over(source).must_equal expect
  end
end
