require 'minitest/autorun'
require_relative '../lib/binary'

describe Binary do
  before do
    @binary = Binary.new
  end

  it 'should be false before binary reached' do
    refute @binary.reached?('foo')
  end

  it 'should be true on binary string occurence' do
    assert @binary.reached?('   <binary>  ')
  end

  it 'should stay true after binary reached' do
    @binary.reached?('   <binary>  ')
    assert @binary.reached?('foo')
  end
end
