require_relative 'string_helper'

class Phrases
  include StringHelper

  def initialize(dictionary_file)
    @dictionary_file = dictionary_file
  end

  def replace(string)
    regexps.each { |r| string = string.gsub(*r) }
    string
  end

  def regexps
    with_capitals.map do |pattern, replacement|
      escaped_string  = '\b' + Regexp.escape(pattern)
      escaped_string += '\b' unless is_punctuation?(pattern[-1])
      [Regexp.new(escaped_string), replacement]
    end
  end

  private

    def phrases
      @phrases ||= parse_phrases(@dictionary_file)
    end

    def with_capitals
      phrases.flat_map do |from_str, to_str|
        from = [from_str, from_str.capitalize]
        to   = [to_str,   to_str.capitalize]
        from.zip(to)
      end
    end

    def parse_phrases(file)
      File.foreach(file)
        .map(&:chomp)
        .reject { |line| is_meaningless?(line) }
        .collect { |line| line.split(/\s*->\s*/) }
    end
end
