require 'io/console'

class Console
  attr_reader :width

  def initialize
    @width = update_width
    @cursor_visible = true
  end

  def update_width
    @width = IO.console.winsize.last
  end

  def hide_cursor
    print "\e[?25l"
    @cursor_visible = false
  end

  def show_cursor
    print "\e[?25h"
    @cursor_visible = true
  end

  def cursor_visible?
    @cursor_visible
  end

  def clear
    print "\e[H\e[2J"
  end
end
