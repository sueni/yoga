require 'yaml'

module Serializer
  extend self

  def unpack(yaml_fname)
    yaml_fname, marsh_fname = %w[yo.yml yo.marsh].map do |d|
      File.join(__dir__, '../dict', d)
    end

    yaml_mtime = File.mtime(yaml_fname)

    unless File.exist?(marsh_fname) and
        File.mtime(marsh_fname) == yaml_mtime
      update_marsh(marsh_fname, yaml_fname, yaml_mtime)
    end

    Marshal.load(File.open(marsh_fname))
  rescue TypeError => e
    puts e
    update_marsh(marsh_fname, yaml_fname, yaml_mtime)
    Marshal.load(File.open(marsh_fname))
  end

  def update_marsh(marsh_fname, yaml_fname, yaml_mtime)
    puts Colors.info.("Создание или обновление словаря...")

    yo = YAML.load_file(yaml_fname)
    File.open(marsh_fname, 'w') { |f| Marshal.dump(yo, f) }
    File.utime(0, yaml_mtime, marsh_fname)
  end
end
