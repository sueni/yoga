class Binary
  def initialize
    @reached = false
  end

  def reached?(line)
    @found ||= line.include?('<binary')
  end
end
