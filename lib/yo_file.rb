require_relative 'string_helper'
require_relative 'yo_proxy'
require_relative 'phrases'

class Yo_file
  include StringHelper

  def initialize(infile, console)
    abort Colors.error.('Файл не найден: ') + infile unless File.exist?(infile)

    @infile            = infile
    @infile_len        = count_lines(@infile)
    @clear_full_screen = true
    @min_context_lvl   = 0
    @prev_key_press    = Time.now
    @console           = console
    @ph = Phrases.new(File.join(__dir__, '../dict/phrases.conf'))
  end

  def process_infile
    handle_outfile

    @yo = YoProxy.new(Serializer.unpack('yo.yml'))
    binary = Binary.new

    begin
      File.foreach(@infile).with_index(1) do |line, lineno|
        next if lineno < @start_from_line

        if binary.reached?(line)
          write_line(line)
        else
          regexped  = RegexpYo.turn_over(line)
          phrased   = @ph.replace(regexped)
          @chunks   = split_by_words(phrased)
          yoficated = words_yo(@chunks, lineno).join
          write_line clean_forced_e_marks(yoficated)
          @io_out.flush
        end
      end
    ensure
      @io_out.close
    end
  end

  private

    def build_outfile_name(suffix)
      File.basename(@infile, '.*') + '.yo' + File.extname(@infile)
    end

    def handle_outfile
      outfile     = build_outfile_name('.yo')
      outfile_len = count_lines(outfile)

      case outfile_len[:text]
      when 0
        @start_from_line = 1
        @io_out = File.open(outfile, 'w')
      when 1...@infile_len[:text]
        puts Colors.info.("Продолжаю запись в недописанный файл: ") + outfile

        @start_from_line = outfile_len[:text] + 1
        @io_out = File.open(outfile, 'a')
      else
        abort(Colors.error.('Выходной файл завершён. Прекращаю работу.'))
      end
    end

    def count_lines(fname)
      memo = {full: 0, text: 0, binary: 0}

      if File.exist?(fname)
        binary = Binary.new

        File.foreach(fname) do |line|
          memo[:full] += 1
          memo[:text] += 1 unless binary.reached?(line)
        end
      end
      memo
    end

    def ask(word, replacement, idx, lineno, context_lvl=@min_context_lvl)
      @console.clear if @clear_full_screen
      @console.hide_cursor if @console.cursor_visible?

      display_header(lineno, context_lvl, replacement)

      puts wrap_long_string(get_context(word, idx, lineno, context_lvl),
                            @console.width)

      @clear_full_screen = true

      case answer = read_answer(context_lvl)
      when 'f'
        ask(word, replacement, idx, lineno, context_lvl+=1)
      when 'F'
        ask(word, replacement, idx, lineno, context_lvl+=1)
        @min_context_lvl += 1
      else
        answer
      end
    end

    def read_answer(lvl)
      answer=''

      loop do
        answer = STDIN.getch

        # protect from finger trembling
        gone = Time.now - @prev_key_press
        if gone < 0.6
          print Colors.error.("Надо выждать %.2f сек.\r" % (0.6 - gone))

          @prev_key_press = Time.now
          redo
        else
          @prev_key_press = Time.now
        end

        case answer
        when /[YyNn]/
          break answer
        when /[fF]/
          break answer if lvl < 2
        when "\u0003"
          exit
        end
      end
    end

    def words_yo(chunks, lineno)
      chunks.each_with_index.map do |chunk, idx|
        next chunk unless is_word?(chunk)

        word        = chunk
        ws          = word.downcase
        replacement = @yo[ws]

        next word unless replacement

        origin_case = get_case(word)

        if is_ambiguous?(replacement)
          replacement = replacement[1..-1]

          case ask(word, replacement, idx, lineno)
          when 'y'
            replacement.public_send(origin_case)
          when 'Y'
            @yo[ws] = replacement
            replacement.public_send(origin_case)
          when 'n'
            word
          when 'N'
            @yo.delete(ws)
            word
          end
        else
          @yo[ws].public_send(origin_case)
        end
      end
    end

    def write_line(line)
      @io_out.puts line
    end

    def get_wrapping_lines(lineno, lvl: 1)
      IO.
        foreach(@infile).
        with_index(1).
        select do |_, i|

          ((lineno-lvl)..(lineno+lvl)).include?(i) and i != lineno

        end.map { |m| m.first.chomp }
    end

    def get_context(word, idx, lineno, context_lvl)
      chunks      = @chunks[0..-1] # create copy of the list to modify
      chunks[idx] = Colors.candidate.(word)

      from_idx, to_idx = calculate_context_range(idx, chunks.size, 9)

      whole_line = chunks.join.strip

      # don't shorten line if it's short already
      if @chunks[0..-1].join.strip.size < 67
        context_lvl += 1
      else
        shortened_line = chunks[from_idx..to_idx].join.strip
      end

      case context_lvl
      when 0 then " %s" % shortened_line
      when 1 then "%s" % whole_line
      else
        wrapping_lines = get_wrapping_lines(lineno, lvl: context_lvl-1)

        top, bottom = wrapping_lines.each_slice(wrapping_lines.size/2).to_a
        [top.map(&:strip), chunks.join.strip, bottom.map(&:strip)].join("\n")
      end
    end

    def calculate_context_range(idx, size, decor)
      from_idx = (idx - decor < 0 ? 0 : idx - decor)
      to_idx = (idx + decor > size ? size : idx + decor)
      [from_idx, to_idx]
    end

    def display_header(lineno, context_lvl, replacement)
      rel_pos = '%d%%' % (lineno.to_f / @infile_len[:text] * 100)
      abs_pos = '%s' % lineno

      line_1_left  = ' %s - заменить   '\
        '(%s - и запомнить)  %s - сохранить и выйти' %
        [Colors.bold.(?y), Colors.bold.(?Y), Colors.bold.('CTRL-C')]

      line_1_right = ' Готово: %s ' % rel_pos.ljust(6)
      line_2_left  = ' %s - пропустить (%s - и запомнить)' %
        [Colors.bold.(?n), Colors.bold.(?N)]
      line_2_right = ' Строка: %s ' % abs_pos.ljust(6)

      line_3 = if context_lvl < 2
                 ' %s - + контекст (%s - и запомнить)' %
                   [Colors.bold.(?f), Colors.bold.(?F)]
               else
                 Colors.warning.(' Предельный уровень контекста!')
               end

      bottom_pad   = '| %s |%s' % [replacement, ?-]

      puts ?- * @console.width

      puts(line_1_left +
           ' ' * (@console.width - Colors.strip(line_1_left).size -
                  line_1_right.size) + line_1_right)

      puts(line_2_left +
           ' ' * (@console.width - Colors.strip(line_2_left).size -
                  line_2_right.size) + line_2_right)

      puts(line_3)

      puts(?- * ((@console.width - bottom_pad.size) * 0.62) +
           bottom_pad +
           ?- * ((@console.width - bottom_pad.size) * 0.38 + 1))
    end
end
