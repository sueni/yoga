# Class to read input from stdin instead of a file
class Yo_stdin < Yo_file
  def initialize
    @io_in  = $stdin
    @io_out = $stdout
    @ph = Phrases.new(File.join(File.dirname($0), 'dict/phrases.conf'))
  end

  def process_stdin
    @yo = YoProxy.new(Serializer.unpack('yo.yml'))
    prompt

    while @io_in.gets
      regexped  = RegexpYo.turn_over($_)
      phrased   = phrases_yo(regexped)
      chunks    = split_by_words(phrased)
      yoficated = words_yo(chunks).join
      write_line clean_forced_e_marks(yoficated)
      prompt
    end
  end

  private

    def prompt
      print("\n ┌─┤ ")
    end

    def write_line(line)
      print '└─> '.rjust(5)
      super
    end

    def phrases_yo(line)
      @ph.regexps.each do |pattern, replacement|
        if pattern.match? Colors.strip(line)
          line = Colors.strip(line).gsub(pattern, Colors.phrase.(replacement))
        end
      end
      line
    end

    def words_yo(chunks)
      chunks.each_with_index.map do |chunk, ind|
        next chunk unless is_word?(chunk)

        word        = chunk
        ws          = word.downcase
        replacement = @yo[ws]

        next word unless replacement

        origin_case = get_case(word)

        if is_ambiguous?(replacement)
          Colors.candidate.(word)
        else
          Colors.yo.(replacement.public_send(origin_case))
        end
      end
    end
end
