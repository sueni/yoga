module StringHelper
  module_function

  def split_by_words(str)
    str.split(/(\p{Word}+)/)
  end

  def is_word?(str)
    /\p{Word}/.match? str
  end

  def get_case(str)
    case str
      when str.downcase then :downcase
      when str.capitalize then :capitalize
      when str.upcase then :upcase
    else
      nil
    end
  end

  def wrap_long_string(str, width)
    str.gsub(/(.{1,#{width}})(\s+|\Z)/, "\\1\n")
  end

  def is_ambiguous?(str)
    str.chr == ??
  end

  def is_meaningless?(str)
    str.start_with?('#') or /^\s*$/.match? str
  end

  def is_punctuation?(str)
    /\p{Punct}/.match? str
  end

  def clean_forced_e_marks(str)
    str.gsub('е̊', 'е')
  end
end
