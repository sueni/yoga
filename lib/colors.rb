# Documentation: https://github.com/piotrmurach/pastel

require 'pastel'

module Colors
  @pastel = Pastel.new

  extend self

  def info
    @pastel.green.detach
  end

  def warning
    @pastel.inverse.detach
  end

  def error
    @pastel.red.bold.detach
  end

  def candidate
    @pastel.yellow.bold.detach
  end

  def phrase
    @pastel.bright_blue.detach
  end

  def yo
    @pastel.bright_green.detach
  end

  def bold
    @pastel.bold.detach
  end

  def strip(string)
    @pastel.strip(string)
  end
end
