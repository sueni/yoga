require_relative 'prefixes'
require_relative 'prefix_exclusions'

class YoProxy
  def initialize(dictionary)
   @dictionary = dictionary
   @prefixes = Prefixes.new.prefixes
   @prefix_exclusions = PrefixExclusions.new
  end

  def [](key)
    @dictionary.fetch(key) do |missed_key|
      @prefixes.each do |prefix|
        if value = @dictionary[missed_key.sub(%r"^#{prefix}", '')]
          return @prefix_exclusions.pick(missed_key, value, prefix)
        end
      end
      nil
    end
  end

  def []=(key, value)
    @dictionary[key] = value
  end

  def delete(key)
    @dictionary.delete(key)
  end
end
