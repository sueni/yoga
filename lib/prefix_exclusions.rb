class PrefixExclusions
  def initialize
    file_of_prefix_exclusions = File.join(File.dirname(__FILE__),
                                            '../dict/prefix_exclusions')
    @exclusions = File.readlines(file_of_prefix_exclusions).map(&:chomp)
  end

  def pick(exclusion, alternative, prefix)
    if @exclusions.include?(exclusion) or alternative.chr == ??
      exclusion
    else
      prefix + alternative
    end
  end
end
