class RegexpYo < String
  def self.turn_over(string, mode=:file)
    string = RegexpYo.new(string)
    RegexpYo.instance_methods(false).each do |method|
      string = string.public_send(method)
    end
    string
  end

  def trex_chet
    gsub(/(?<=\b[Тт]р|\b[Чч]етыр)е(?=х)/, 'ё')
  end

  def priyomnik
    gsub('приемник', 'приёмник').gsub('Приемник', 'Приёмник')
  end
end
