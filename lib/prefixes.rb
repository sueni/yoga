class Prefixes
  attr_reader :prefixes

  def initialize
    file_of_prefixes = File.join(File.dirname(__FILE__), '../dict/prefixes')
    @prefixes = File.readlines(file_of_prefixes).map(&:chomp)
  end
end
