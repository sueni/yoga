#!/usr/bin/ruby
# frozen_string_literal: true

%w{
  colors
  console
  binary
  regexp_yo
  serializer
  yo_file
  yo_stdin
}.each do |m|
  require_relative File.join('lib', m)
end

trap ("INT") do
  abort('TRL-C')
end

console = Console.new

trap('WINCH') do
  console.update_width
end

unless ARGV.empty?
  until ARGV.empty?
    yo = Yo_file.new(ARGV.shift, console)
    begin
      yo.process_infile
    ensure
      console.show_cursor
    end
  end
else
  yo = Yo_stdin.new
  yo.process_stdin
end
